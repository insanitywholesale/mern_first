const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const MessageSchema = new Schema ({
	//id: {
	//	type: mongoose.Schema.Types.ObjectId,
	//	index: true,
	//	required: true,
	//	auto: true
	//},
	user: {
		type: String,
		required: true
	},
	content: {
		type: String,
		default: "I should never see this"
	}
});

module.exports = Message = mongoose.model('message', MessageSchema);
