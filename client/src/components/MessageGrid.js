import React, { Component } from 'react';
import { Container, Button } from 'reactstrap';
import { Card, CardText, CardBody, CardTitle } from 'reactstrap';
import { connect } from 'react-redux';
import { getMessages, deleteMessage } from '../actions/messageActions';
import PropTypes from 'prop-types';

class MessageGrid extends Component {

componentDidMount() {
	this.props.getMessages();
}

onDeleteClick = id => {
	this.props.deleteMessage(id);
}


render() {
	const { messages } = this.props.message;
	return (
		<Container>
		<div className="row">
			{messages.map(({ _id, user, content }) => (
				<Container className="col-lg-3 col-md-6 col-sm-12">
					<Card className="mb-4">
						<CardBody>
							<CardTitle>From {user}:</CardTitle>
      						<CardText>{content}</CardText>
						<Button
							className="remove-btn m-1"
							color="danger"
							size="sm"
							float="right"
							onClick={this.onDeleteClick.bind(this, _id)}
						>
						Delete
						</Button>
						</CardBody>
					</Card>
				</Container>
			))}
		</div>
		</Container>
	);
}

}

MessageGrid.propTypes = {
	getMessages: PropTypes.func.isRequired,
	message: PropTypes.object.isRequired
}

const mapStateToProps = (state) => ({
	message: state.message
});

export default connect(mapStateToProps, { getMessages, deleteMessage })(MessageGrid);
