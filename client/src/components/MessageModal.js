import React, { Component } from 'react';
import {
	Button,
	Container,
	Modal,
	ModalHeader,
	ModalBody,
	Form,
	FormGroup,
	Label,
	Input
} from 'reactstrap';
import { connect } from 'react-redux';
import { addMessage } from '../actions/messageActions';


class MessageModal extends Component {
	state = {
		modal: false,
		content: ''
	}

	toggle = () => {
		this.setState({
			modal: !this.state.modal
		});
	}

	onChange = e => {
		this.setState({ [e.target.name]: e.target.value });
	}

	onSubmit = e => {
		e.preventDefault();
		const newMsg = {
			user: 'guest',
			content: this.state.content
		};
		// add message to messages
		this.props.addMessage(newMsg, false);
		// close modal
		this.toggle();
	}

	render() {
		return (
			<div>
			<Container>
				<Container> {/*container exists so the "Add Message button is aligned and isn't more to the left than the Message List*/}
				<Button
					color="dark"
					style={{marginBotton: '10px'}}
					onClick={this.toggle}
				>
				Add Message
				</Button>
				</Container>
				<Modal
					isOpen={this.state.modal}
					toggle={this.toggle}
				>
					<ModalHeader toggle={this.toggle}>Add To Messages</ModalHeader>
					<ModalBody>
						<Form onSubmit={this.onSubmit}>
							<FormGroup>
								<Label for="message">Message</Label>
								<Input
									type="text"
									name="content" // value matches content in the message state
									id="message"
									placeholder="Message Goes Here"
									onChange={this.onChange}
								>
								</Input>
								<Button color="dark" style={{marginTop: '2rem'}} block>
									Add Message
								</Button>
							</FormGroup>
						</Form>
					</ModalBody>
				</Modal>
			</Container>
			<br/>
			</div>
		);
	}
		
}

const mapStateToProps = state => ({
	message: state.message
});

export default connect(mapStateToProps, { addMessage })(MessageModal);
