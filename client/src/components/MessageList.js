import React, { Component } from 'react';
import { Container, ListGroup, ListGroupItem, Button } from 'reactstrap';
import { CSSTransition, TransitionGroup } from 'react-transition-group';
import { connect } from 'react-redux';
import { getMessages, deleteMessage } from '../actions/messageActions';
import PropTypes from 'prop-types';

class MessageList extends Component {

componentDidMount() {
	this.props.getMessages();
}

onDeleteClick = (id) => {
	this.props.deleteMessage(id);
}

render() {
	const { messages } = this.props.message;
	return (
		<Container>
		<div className="mb-4">
			<ListGroup>
				<TransitionGroup className="message-list">
					{messages.map(({ _id, user, content }) => (
						<CSSTransition key={_id} timeout={500} classNames="fade">
							<ListGroupItem className="d-flex justify-content-between align-items-center">
								{user} says: {content}
								<span className="d-flex justify-content-between align-items-center my-auto text-right">
									<Button
										className="remove-btn m-1"
										color="danger"
										size="sm"
										float="right"
										onClick={this.onDeleteClick.bind(this, _id)}
									>
										X
									</Button>
								</span>
							</ListGroupItem>
						</CSSTransition>
					))}
				</TransitionGroup>
			</ListGroup>
		</div>
		</Container>
	);
}

		
}

MessageList.propTypes = {
	getMessages: PropTypes.func.isRequired,
	message: PropTypes.object.isRequired
}

const mapStateToProps = (state) => ({
	message: state.message
});

export default connect(mapStateToProps, { getMessages, deleteMessage })(MessageList);
