import React, { Component } from 'react';
import { Button, Container } from 'reactstrap';
import MessageGrid from './MessageGrid';
import MessageList from './MessageList';

class ViewToggle extends Component {
	state = {
		isList: false
	}

	toggle = () => {
		this.setState({
			isList: !this.state.isList
		})
	}

	render() {
		return (
			<Container>
			<Container>
				<Button color="dark" size="md" onClick={this.toggle} block>
					Change to {this.state.isList ? 'Grid' : 'List'}
				</Button>
			</Container>
			<br/>
			<Container>
				{!this.state.isList && <MessageGrid />}
				{this.state.isList && <MessageList />}
			</Container>
			</Container>
		)
	}

}


export default ViewToggle;
