import { GET_MESSAGES, ADD_MESSAGE, DELETE_MESSAGE, MESSAGES_LOADING } from './types';
import axios from 'axios';

const wsURL = window._env_.WS_URL || 'ws://localhost:7070/sockjs-node'
const server = new WebSocket(wsURL);
//const API_URL = 'https://mern.inherently.xyz/api/messages'
const API_URL = window._env_.API_URL || 'http://localhost:5000/api/messages'
console.log("wsURL " + wsURL);
console.log("apiURL " + API_URL);
let lastMessageID;

export const getMessages = () => dispatch => {
	dispatch(setMessagesLoading());
	axios
		.get(API_URL)
		.then(res => dispatch({
			type: GET_MESSAGES,
			payload: res.data
		})
	);

	server.addEventListener('message', message => {
		let msgdata = JSON.parse(message.data)

		//adding message
		if (msgdata.type === 'message') {
			if (msgdata._id !== lastMessageID) {
				dispatch({
					type: ADD_MESSAGE,
					payload: msgdata
				});
				lastMessageID = msgdata._id;
			}
		}

		//deleting message
		if (msgdata.type === 'id') {
			dispatch({
				type: DELETE_MESSAGE,
				payload: msgdata
			});
			lastMessageID = msgdata.id;
		}
	});
};

export const addMessage = (message, shouldDispatch = true) => dispatch => {
	axios
		.post(API_URL, message)
		.then(res => {
			if (shouldDispatch) {
				dispatch({
					type: ADD_MESSAGE,
					payload: res.data
				})
			}
		}
	);
	return {
		type: ADD_MESSAGE,
		payload: message
	};
};

export const deleteMessage = (id, shouldDispatch = true) => dispatch => {
	axios
		.delete(`${API_URL}/${id}`)
		.then(res => {
			if (shouldDispatch) {
				dispatch({
					type: DELETE_MESSAGE,
					payload: res.data
				})
			}
		}
	);
};

export const setMessagesLoading = () => {
	return {
		type: MESSAGES_LOADING
	};
};
