import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import AppNavbar from './components/AppNavbar';
//import MessageList from './components/MessageList';
//import MessageGrid from './components/MessageGrid';
import MessageModal from './components/MessageModal';
import ViewToggle from './components/ViewToggle';
import { Provider } from 'react-redux';
import store from './store';
import { Container } from 'reactstrap';

function App() {
  return (
	<Provider store={store}>
    	<div className="App">
			<AppNavbar />
			<Container>
				<h3>WS_URL: {window._env_.WS_URL}</h3>
				<h3>API_URL: {window._env_.API_URL}</h3>
			</Container>
			<MessageModal />
			<ViewToggle />
    	</div>
	</Provider>
  );
}

export default App;
