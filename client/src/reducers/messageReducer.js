import { GET_MESSAGES, ADD_MESSAGE, DELETE_MESSAGE, MESSAGES_LOADING } from '../actions/types';

const initialState = {
	messages: [],
	loading: false
};

export default function(state = initialState, action) {
	switch(action.type) {
		case GET_MESSAGES:
			return {
				...state,
				messages: action.payload,
				loading: false
			};
		case DELETE_MESSAGE:
			return {
				...state,
				messages: state.messages.filter(message => message._id !== action.payload.id)
			};
		case ADD_MESSAGE:
			return {
				...state,
				messages: [action.payload, ...state.messages]
			};
		case MESSAGES_LOADING:
			return {
				...state,
				loading: true
			};
		default:
			return state;
	}
}
