#!/bin/sh -x

[ -z "$WS_HOST" ] && WS_HOST=localhost
[ -z "$WS_PORT" ] && WS_PORT=7070
[ -z "$WS_URL" ] && WS_URL=ws://${WS_HOST}:${WS_PORT}/sockjs-node
[ -z "$API_HOST" ] && API_HOST=localhost
[ -z "$API_PORT" ] && API_PORT=5000
[ -z "$API_URL" ] && API_URL=http://${API_HOST}:${API_PORT}/api/messages

# Recreate config file
rm -rf ./env-config.js
touch ./env-config.js

# Add assignment
echo "window._env_ = {" >> ./env-config.js

[ "$WS_HOST" != "localhost" ] && WS_HOST=$(getent hosts "$WS_HOST" | cut -f 1 | awk '{print $1}')
WS_PORT=7070
WS_URL=ws://${WS_HOST}:${WS_PORT}/sockjs-node
[ "$API_HOST" != "localhost" ] && API_HOST=$(getent hosts "$API_HOST" | cut -f 1 | awk '{print $1}')
API_PORT=5000
API_URL=http://${API_HOST}:${API_PORT}/api/messages

echo "  WS_HOST: \"$WS_HOST\"," >> ./env-config.js
echo "  WS_PORT: \"$WS_PORT\"," >> ./env-config.js
echo "  WS_URL: \"$WS_URL\"," >> ./env-config.js
echo "  API_HOST: \"$API_HOST\"," >> ./env-config.js
echo "  API_PORT: \"$API_PORT\"," >> ./env-config.js
echo "  API_URL: \"$API_URL\"," >> ./env-config.js

## Read each line in .env file
## Each line represents key=value pairs
#while read -r line || [ -n "$line" ];
#do
#	# Split env variables by character `=`
#	if printf '%s\n' "$line" | grep -q -e '='; then
#		varname=$(printf '%s\n' "$line" | sed -e 's/=.*//')
#		varvalue=$(printf '%s\n' "$line" | sed -e 's/^[^=]*=//')
#	fi
#
#	# read value of current variable if exists as environment variable
#	value=$(eval echo "\$$varname" | cut -f 1)
#	# otherwise use value from .env file
#	[ -z $value ] && value=${varvalue}
#
#	# Append configuration property to JS file
#	echo "  $varname: \"$value\"," >> ./env-config.js
#done < .env

echo "}" >> ./env-config.js
