#!/bin/bash

# Recreate config file
rm -rf ./env-config.js
touch ./env-config.js

# Add assignment
echo "window._env_ = {" >> ./env-config.js

# Read each line in .env file
# Each line represents key=value pairs
while read -r line || [ -n "$line" ];
do
	# Split env variables by character `=`
	if printf '%s\n' "$line" | grep -q -e '='; then
		varname=$(printf '%s\n' "$line" | sed -e 's/=.*//')
		varvalue=$(printf '%s\n' "$line" | sed -e 's/^[^=]*=//')
	fi

# kinda does something
	if [ $varname = "WS_URL" ]; then
		export $line
		#eval echo $line
		export "$(eval echo "$line")"
		#eval echo $line
	fi
	if [ $varname = "API_URL" ]; then
		export $line
		#eval echo $line
		export "$(eval echo "$line")"
		#eval echo $line
	fi
# but not well

	# Read value of current variable if exists as Environment variable
	value=$(eval echo "\$$varname" | cut -d ' ' -f 1)
	# Otherwise use value from .env file
	[ -z $value ] && value=${varvalue}
	
	if [ "$varname" = "WS_HOST" ] && [ "$WS_HOST" != "localhost" ]; then
		res_val_ws="$(getent hosts "$WS_HOST" | cut -f 1 | awk '{print $1}' )"
		#echo $res_val_ws
		export "${varname}=${res_val_ws}"
	elif [ "$varname" = "API_HOST" ] && [ "$API_HOST" != "localhost" ]; then
		res_val_api="$(getent hosts "$API_HOST" | cut -f 1 | awk '{print $1}' )"
		#echo $res_val_api
		export "${varname}=${res_val_api}"
	else
		export "${varname}=${value}"
	fi
	
	# Append configuration property to JS file
	echo "  $varname: \"$value\"," >> ./env-config.js
done < .env

echo "}" >> ./env-config.js
#view raw
