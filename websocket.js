const ws = require('ws');
const port = process.env.WS_PORT || 7070;

const server = new ws.Server({
    port,
    path: '/sockjs-node'
});

broadcast = message => {
    for (const client of server.clients) {
        client.send(message);
    }
}

module.exports = {
    broadcast,
    server,
    websocketPort: port
};
