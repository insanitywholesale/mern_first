const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const path = require('path');
const cors = require('cors');
const ws = require('ws');


const app = express();

// WebSocket-related, allows cross origin resource requests
app.use(cors());
app.use(bodyParser.urlencoded({extended: false}));

const messages = require("./routes/api/messages");
// BodyParser middleware. apparently needed
app.use(bodyParser.json());

// MongoDB configuration
const db = require("./config/keys").mongoURI;

// Connect to MongoDB
let mong = mongoose
	.connect(db, { useNewUrlParser: true, useFindAndModify: false, useUnifiedTopology: true, useCreateIndex: true })
	.then(() => console.log("MongoDB Connection Established"))
	.catch(err => console.log(`Error: ${err}`));

// Use routes
app.use("/api/messages", messages);

if(process.env.NODE_ENV === 'production') {
	// where to find static assets
	app.use(express.static('client/build'));
	// redirect all requests to the frontend
	app.get('/*', (req, res) => {
		res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'));
	});
}

// Define port
const port = process.env.PORT || 5000;

app.listen(port, () => console.log(`Server started on port ${port}`));

module.exports = { mong }
