const express = require("express");
const router = express.Router();
const { server, broadcast } = require('../../websocket');
const mongoose = require('mongoose');

// Message model

const Message = require("../../models/Message");

// @route GET api/messages
// @desc get all messages
// @access public for now
router.get("/", (req, res) => {
	Message.find()
		.sort({_id: -1})
		.then(messages => res.json(messages))
});

// @route POST api/messages
// @desc create a message
// @access public for now
router.post("/", (req, res) => {
	const { user, content } = req.body;
	const thisid = mongoose.Types.ObjectId();
	const newMessage = new Message({
		_id: thisid,
		type: 'message',
		user,
		content
	});
	newMessage.save().then(message => res.json(message));
	broadcast(JSON.stringify({
		_id: thisid,
		type: 'message',
		user,
		content
	}));
});

// @route DELETE api/messages
// @desc delete a message
// @access public for now
router.delete("/:id", (req, res) => {
	broadcast(JSON.stringify({
		type: "id",
		id: req.params.id
	}));
	Message.findById(req.params.id)
		.then(item => item.remove().then(() => res.json({success: true})))
		.catch(err => res.status(404).json({success: false}));
})

module.exports = router;
